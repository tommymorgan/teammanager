package com.tommymorgan.teammanager.dto;

import com.tommymorgan.teammanager.domain.Team;

/**
 * Created by tommy on 3/6/16.
 */
public class SeasonDTO extends com.tommymorgan.teammanager.domain.Season {
	public SeasonDTO(com.tommymorgan.teammanager.domain.Season season, Iterable<Team> teams) {
		this.setId(season.getId());
		this.setName(season.getName());
		this.teams = teams;
	}

	public Iterable<Team> getTeams() {
		return teams;
	}

	public void setTeams(Iterable<Team> teams) {
		this.teams = teams;
	}

	private Iterable<Team> teams;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		SeasonDTO that = (SeasonDTO) o;

		return getTeams().equals(that.getTeams());

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + getTeams().hashCode();
		return result;
	}
}
