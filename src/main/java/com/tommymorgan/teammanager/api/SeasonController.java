package com.tommymorgan.teammanager.api;

import com.tommymorgan.teammanager.dao.GameRepository;
import com.tommymorgan.teammanager.dao.SeasonRepository;
import com.tommymorgan.teammanager.dao.TeamRepository;
import com.tommymorgan.teammanager.dto.SeasonDTO;
import com.tommymorgan.teammanager.domain.Game;
import com.tommymorgan.teammanager.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by tommy on 1/13/16.
 */
@RestController
public class SeasonController {
	@Autowired
	SeasonRepository seasonRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	TeamRepository teamRepository;

	@RequestMapping("/seasons")
	public Iterable<com.tommymorgan.teammanager.domain.Season> seasons(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return seasonRepository.findAll();
	}

	@RequestMapping(value = "/seasons/{id}", method = RequestMethod.GET)
	public com.tommymorgan.teammanager.domain.Season season(@PathVariable long id) {
		return seasonRepository.findOne(id);
	}

	@RequestMapping(value = "/seasons/{id}/dto", method = RequestMethod.GET)
	public SeasonDTO seasonDetailed(@PathVariable long id) {
		com.tommymorgan.teammanager.domain.Season season = seasonRepository.findOne(id);
		Iterable<Team> teams = teamRepository.findBySeasonId(id);
		return new SeasonDTO(season, teams);
	}

	@RequestMapping(value = "/seasons/{id}", method = RequestMethod.PUT)
	public com.tommymorgan.teammanager.domain.Season season(@PathVariable long id, @RequestBody com.tommymorgan.teammanager.domain.Season season) {
		return seasonRepository.save(season);
	}

	@RequestMapping(value = "/seasons/{id}/games", method = RequestMethod.GET)
	public Iterable<Game> gamesBySeasonId(@PathVariable long id) {
		return gameRepository.findBySeasonId(id);
	}

	@RequestMapping(value = "/seasons/{id}/teams", method = RequestMethod.GET)
	public Iterable<Team> teamsBySeasonId(@PathVariable long id) {
		return teamRepository.findBySeasonId(id);
	}
}
