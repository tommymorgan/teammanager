package com.tommymorgan.teammanager.api;

import com.tommymorgan.teammanager.dao.GameRepository;
import com.tommymorgan.teammanager.dao.PlayerRepository;
import com.tommymorgan.teammanager.dao.TeamRepository;
import com.tommymorgan.teammanager.domain.Game;
import com.tommymorgan.teammanager.domain.Player;
import com.tommymorgan.teammanager.domain.Season;
import com.tommymorgan.teammanager.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by tommy on 1/13/16.
 */
@RestController
public class TeamController {
	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	TeamRepository teamRepository;

	@RequestMapping("/teamsBySeason")
	public Iterable<Team> teamsBySeason(@RequestParam("seasonId") long seasonId, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return teamRepository.findBySeasonId(seasonId);
	}

	@RequestMapping("/teamsByPlayer")
	public Iterable<Team> teamsByPlayer(@RequestParam("playerId") long playerId, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return teamRepository.findByPlayerId(playerId);
	}

	@RequestMapping("/teams/{id}")
	public Team team(@PathVariable long id) {
		return teamRepository.findOne(id);
	}

	@RequestMapping("/teams/{id}/players")
	public Iterable<Player> players(@PathVariable long id) {
		return playerRepository.findByTeamId(id);
	}

	@RequestMapping("/teams/{id}/games")
	public Iterable<Game> games(@PathVariable long id) {
		return gameRepository.findByTeamId(id);
	}
}
