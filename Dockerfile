FROM java:8-jre
WORKDIR /home/teammanager
ADD build/libs/teammanager-0.0.1.jar teammanager.jar
EXPOSE 8000
CMD ["java","-jar","teammanager.jar"]
